import { Injectable } from '@angular/core';
import { Observable, Subscriber } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { _ } from 'underscore'


@Injectable({
  providedIn: 'root'
})
export class MyserviceService {

  all = null
  variableGeneral = 'all'
  constructor(private httpClient: HttpClient) { }

  /**
   * Obtiene todos los post que existen en la api
   */
  initGetPost(): Observable<any> {
    return new Observable((observer: Subscriber<any>) => {
      this.httpClient.get<any>(`https://jsonplaceholder.typicode.com/posts`).subscribe(data => {
        observer.next(data);
        observer.complete();
      }); // fin 1er Subscribe
    });
  } // 


  /**
   * Obtiene todos usuarios que existen
   */
  initGetArrayUsers(): Observable<any> {
    return new Observable((observer: Subscriber<any>) => {
      this.httpClient.get<any>(`https://jsonplaceholder.typicode.com/posts`).subscribe(postsAll => {
        // agrupando por userId y generando array de usuarios
        let usersArray = _.keys(_.groupBy(postsAll, function (post) { return post.userId }));
        observer.next(usersArray);
        observer.complete();
      }); // fin 1er Subscribe
    });
  } // 


  /**
   * 
   * @param userId      numero  ej: 10
   * 
   */
  initGetfromUser(userId: number): Observable<any> {
    return new Observable((observer: Subscriber<any>) => {
      let urlBase = `https://jsonplaceholder.typicode.com/users/${userId}`
      this.httpClient.get<any>(`${urlBase}/posts`).subscribe(posts => {
        this.httpClient.get<any>(`${urlBase}/todos`).subscribe(todos => {
          this.httpClient.get<any>(`${urlBase}/albums`).subscribe(albums => {
            observer.next({ posts, todos, albums, profile: { name: null, email: null, address: null } });
            observer.complete();
          });
        });
      });
    });
  }

  /**
   *  Obtiene todos los datos iniciales y se almacenan en la variable all de este servicio
   */
  initGetAllPostTodosAlbums() {
    return new Observable((observer: Subscriber<any>) => {
      this.initGetArrayUsers().subscribe(usersArray => {
        let result = {}
        let processed = 0
        usersArray.forEach(user => {
          this.initGetfromUser(user).subscribe(data => {
            processed++
            result[user] = data;
            if (processed >= usersArray.length) {
              observer.next(result);
              observer.complete()
            }
          })
        });
      });
    });

  }


  initAll(restart?: any): Observable<any> {
    return new Observable((observer: Subscriber<any>) => {
      if (restart) {
        localStorage.removeItem(this.variableGeneral)
        console.log('Reseteando Variable general')
      }
      let allOnStorage = JSON.parse(localStorage.getItem(this.variableGeneral));
      if (!allOnStorage) {
        console.log('No hay  Variable General, creandola..');

        this.initGetAllPostTodosAlbums().subscribe(data => {
          this.all = data
          localStorage.setItem(this.variableGeneral, JSON.stringify(data));
          console.log('actualizando usuario en Storage variableGeneral');
          observer.next(data)
        })

      } else {
        console.log('si existe, utilizando');
        this.all = JSON.parse(localStorage.getItem(this.variableGeneral))
        observer.next(this.all)

      }
    });
  } // 


  /////////////////////////////////////// sin observable ///////////////////////////

  getUserQuantity() {
    let usersArray = _.keys(JSON.parse(localStorage.getItem(this.variableGeneral)));
    return usersArray.length;
  };
  getUserArray() {
    let usersArray = _.keys(JSON.parse(localStorage.getItem(this.variableGeneral)));
    return usersArray;
  };


  getAllPosts() {
    let all = JSON.parse(localStorage.getItem(this.variableGeneral))
    let resultado = []
    this.getUserArray().forEach(user => {
      resultado = [...resultado, ...all[user].posts]
    });
    return resultado
  }

  getAllAlbums() {
    let all = JSON.parse(localStorage.getItem(this.variableGeneral))
    let resultado = []
    this.getUserArray().forEach(user => {
      resultado = [...resultado, ...all[user].albums]
    });
    return resultado
  }
  getAllTodos() {
    let all = JSON.parse(localStorage.getItem(this.variableGeneral))
    let resultado = []
    this.getUserArray().forEach(user => {
      resultado = [...resultado, ...all[user].todos]
    });
    return resultado
  }


  getAllPostsQuantity() {
    return this.getAllPosts().length
  }
  getAllAlbumsQuantity() {
    return this.getAllAlbums().length
  }
  getAllTodosQuantity() {
    return this.getAllTodos().length
  }



  getUserPosts(number) {
    let resultado = JSON.parse(localStorage.getItem(this.variableGeneral))
    return resultado[number].posts
  }
  getUserAlbums(number) {
    let resultado = JSON.parse(localStorage.getItem(this.variableGeneral))
    return resultado[number].albums
  }
  getUserTodos(number) {
    let resultado = JSON.parse(localStorage.getItem(this.variableGeneral))
    return resultado[number].todos
  }
  getUserProfile(number) {
    let resultado = JSON.parse(localStorage.getItem(this.variableGeneral))
    return resultado[number].profile
  }


  getUserPostsQuantity(number) {
    let resultado = JSON.parse(localStorage.getItem(this.variableGeneral))
    return resultado[number].posts.length
  }
  getUserAlbumsQuantity(number) {
    let resultado = JSON.parse(localStorage.getItem(this.variableGeneral))
    return resultado[number].posts.length
  }
  getUserTodosQuantity(number) {
    let resultado = JSON.parse(localStorage.getItem(this.variableGeneral))
    return resultado[number].todos.length
  }


  getUserTodosCompleted(number) {
    let resultado = JSON.parse(localStorage.getItem(this.variableGeneral))
    return _.where(resultado[number].todos, { completed: true });
  }
  getUserTodosIncompleted(number) {
    let resultado = JSON.parse(localStorage.getItem(this.variableGeneral))
    return _.where(resultado[number].todos, { completed: false })
  }


  getUserTodosCompletedQuantity(number){
    return this.getUserTodosCompleted(number).length
  }
  getUserTodosIncompletedQuantity(number){
    return this.getUserTodosIncompleted(number).length
  }


  getAllTodosCompleted() {
    let all = JSON.parse(localStorage.getItem(this.variableGeneral))
    let resultado = []
    this.getUserArray().forEach(user => {
      resultado = [...resultado, ...this.getUserTodosCompleted(user)]
    });
    return resultado
  }
  getAllTodosIncompleted() {
    let all = JSON.parse(localStorage.getItem(this.variableGeneral))
    let resultado = []
    this.getUserArray().forEach(user => {
      resultado = [...resultado, ...this.getUserTodosIncompleted(user)]
    });
    return resultado
  }

  
  getAllTodosCompletedQuantity() {
    return this.getAllTodosCompleted().length
  }
  getAllTodosIcompletedQuantity() {
    return this.getAllTodosIncompleted().length
  }


  getUserLast3Posts(number) {
    return _.sortBy(this.getUserPosts(number), 'id').reverse().slice(0, 3)
  }
  getUserLast3Albums(number) {
    return _.sortBy(this.getUserAlbums(number), 'id').reverse().slice(0, 3)
  }
  getUserLast3Todos(number) {
    return _.sortBy(this.getUserTodos(number), 'id').reverse().slice(0, 3)
  }

} // fin servicio
