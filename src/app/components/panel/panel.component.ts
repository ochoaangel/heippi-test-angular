import { Component, OnInit } from '@angular/core';
import * as Highcharts from 'highcharts';
import { _ } from 'underscore'
import { MyserviceService } from 'src/app/services/myservice.service';

@Component({
  selector: 'app-panel',
  templateUrl: './panel.component.html',
  styleUrls: ['./panel.component.css']
})
export class PanelComponent implements OnInit {

  // Highcharts: typeof Highcharts = Highcharts;
  // chartOptions: Highcharts.Options = {
  //   series: [{
  //     data: [1, 2, 3],
  //     type: 'line'
  //   }]
  // }; 
  highcharts = Highcharts;

  chartOptionsUsuario = {
    chart: {
      type: 'column'
    },
    title: {
      text: 'Actividad de cada usuario'
    },
    subtitle: {
      text: ''
    },
    xAxis: {
      categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
      crosshair: true
    },
    yAxis: {
      min: 0,
      title: {
        text: 'Cantidad de elementos'
      }
    },
    tooltip: {
      headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
      pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
        '<td style="padding:0"><b>{point.y:f}</b></td></tr>',
      footerFormat: '</table>',
      shared: true,
      useHTML: true
    },
    plotOptions: {
      column: {
        pointPadding: 0.2,
        borderWidth: 0
      }
    },
    series: {}
  }

  chartOptionsGeneral = {
    chart: {
      plotBackgroundColor: null,
      plotBorderWidth: null,
      plotShadow: false,
      type: 'pie'
    },
    title: {
      text: 'Actividad General'
    },
    tooltip: {
      pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
    accessibility: {
      point: {
        valueSuffix: '%'
      }
    },
    plotOptions: {
      pie: {
        allowPointSelect: true,
        cursor: 'pointer',
        dataLabels: {
          enabled: true,
          format: '<b>{point.name}</b>: {point.percentage:.1f} %'
        }
      }
    },
    series: [{
      name: 'Brands',
      colorByPoint: true,

      data: [{
        name: 'Chrome',
        y: 61.41,
        sliced: true,
        selected: true
      }, { name: 'Internet Explorer', y: 11.84 },
      {
        name: 'Firefox',
        y: 10.85
      }, {
        name: 'Edge',
        y: 4.67
      }, {
        name: 'Safari',
        y: 4.18
      }, {
        name: 'Sogou Explorer',
        y: 1.64
      }, {
        name: 'Opera',
        y: 1.6
      }, {
        name: 'QQ',
        y: 1.2
      }, {
        name: 'Other',
        y: 2.61
      }]
    }]
  }

  chartOptionsGeneralTareas = {
    chart: {
      plotBackgroundColor: null,
      plotBorderWidth: null,
      plotShadow: false,
      type: 'pie'
    },
    title: {
      text: 'Actividad General'
    },
    tooltip: {
      pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
    accessibility: {
      point: {
        valueSuffix: '%'
      }
    },
    plotOptions: {
      pie: {
        allowPointSelect: true,
        cursor: 'pointer',
        dataLabels: {
          enabled: true,
          format: '<b>{point.name}</b>: {point.percentage:.1f} %'
        }
      }
    },
    series: [{
      name: 'Brands',
      colorByPoint: true,

      data: [{
        name: 'Chrome',
        y: 61.41,
        sliced: true,
        selected: true
      }, { name: 'Internet Explorer', y: 11.84 },
      {
        name: 'Firefox',
        y: 10.85
      }, {
        name: 'Edge',
        y: 4.67
      }, {
        name: 'Safari',
        y: 4.18
      }, {
        name: 'Sogou Explorer',
        y: 1.64
      }, {
        name: 'Opera',
        y: 1.6
      }, {
        name: 'QQ',
        y: 1.2
      }, {
        name: 'Other',
        y: 2.61
      }]
    }]
  }

  ntotalUser
  usuarioMayorPost
  usuarioMayortareasCompletadas
  userPostTabla = [];
  displayedColumns: string[] = ['user', 'name', 'post'];
  constructor(private mys: MyserviceService) { }

  ngOnInit(): void {


    /////////////////////////// Actividad por Usuario /////////////////////////////

    let collection = []
    let arrayUsuarios = this.mys.getUserArray()

    let postByUser = []
    let arrayNombres = []
    let albumsByUser = []
    let todosByUser = []
    let todosCompletedByUser = []
    let todosIncompletedByUser = []


    arrayUsuarios.forEach(usuario => {
      arrayNombres.push(this.mys.getUserProfile(usuario))
      postByUser.push(this.mys.getUserPostsQuantity(usuario));
      todosByUser.push(this.mys.getUserTodosQuantity(usuario));
      todosCompletedByUser.push(this.mys.getUserTodosCompletedQuantity(usuario));
      todosIncompletedByUser.push(this.mys.getUserTodosIncompletedQuantity(usuario));
      albumsByUser.push(this.mys.getUserAlbumsQuantity(usuario));
    });

    // eligiendo texto de nombre para cada usuario
    for (let index = 0; index < arrayUsuarios.length; index++) {
      if (arrayNombres[index].name) {
        arrayNombres[index] = arrayNombres[index].name
      }
      if (!arrayNombres[index].name && arrayNombres[index].email) {
        arrayNombres[index] = arrayNombres[index].email
      }
      if (!arrayNombres[index].name && !arrayNombres[index].email) {
        arrayNombres[index] = arrayUsuarios[index]
      }
    }
    this.chartOptionsUsuario.xAxis.categories = arrayNombres

    collection.push({ name: 'Post', data: postByUser })
    collection.push({ name: 'Álbumes', data: albumsByUser })
    collection.push({ name: 'Tareas', data: todosByUser })
    collection.push({ name: 'Tareas Completadas', data: todosCompletedByUser })
    collection.push({ name: 'Tareas Incompletas', data: todosIncompletedByUser })

    this.chartOptionsUsuario.series = collection

    /////////////////////////// Actividad general /////////////////////////////

    // { name: 'Internet Explorer', y: 11.84 }
    let dataGeneral = []
    dataGeneral.push({ name: 'Total Posts', y: this.mys.getAllPostsQuantity(), sliced: true, selected: true })
    dataGeneral.push({ name: 'Total Albumes', y: this.mys.getAllAlbumsQuantity() })
    dataGeneral.push({ name: 'Total Tareas', y: this.mys.getAllTodosQuantity() })
    console.log('dataGeneral', dataGeneral);
    this.chartOptionsGeneral.series[0].data = dataGeneral

    /////////////////////////// Actividad general Tareas /////////////////////////////

    // { name: 'Internet Explorer', y: 11.84 }
    let dataGeneralTareas = []
    dataGeneralTareas.push({ name: 'Tareas Completadas', y: this.mys.getAllTodosCompletedQuantity(), sliced: true, selected: true })
    dataGeneralTareas.push({ name: 'Tareas Incompletas', y: this.mys.getAllTodosIcompletedQuantity() })
    console.log('dataGeneralTareas', dataGeneralTareas);
    this.chartOptionsGeneralTareas.series[0].data = dataGeneralTareas


    // user,name,post
    let userPostTabla = []
    for (let index = 0; index < arrayUsuarios.length; index++) {
      const element = arrayUsuarios[index];
      userPostTabla.push({ user: arrayUsuarios[index], name: arrayNombres[index], post: this.mys.getUserPostsQuantity(element) })
    }
     

    console.log('userPostTabla',userPostTabla);
    
    this.ntotalUser = this.mys.getUserQuantity()

    let mayorPost=[]
    let mayorTarea=[]
    arrayUsuarios.forEach(element => {
      mayorPost.push({user:element,quantity:this.mys.getUserPostsQuantity(element)})
      mayorTarea.push({user:element,quantity:this.mys.getUserTodosQuantity(element)})
    });
    mayorPost= _.sortBy(mayorPost, 'queantity');
    this.usuarioMayorPost = mayorPost[0]
    mayorTarea= _.sortBy(mayorTarea, 'queantity');
    this.usuarioMayortareasCompletadas = mayorTarea[0]





    // console.log('mayorPost',mayorPost); 

  }// fin ngOnInit

} // fin Class




